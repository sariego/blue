package psapp.io.innovaxxion;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import psapp.io.innovaxxion.model.Measurement;
import psapp.io.innovaxxion.model.Sensor;
import psapp.io.innovaxxion.network.NetworkAPI;
import psapp.io.innovaxxion.network.SensorData;
import psapp.io.innovaxxion.storage.StorageAPI;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SensorsBActivity extends SensorBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors_a);

        findViewById(R.id.linearLayout1).setVisibility(View.VISIBLE);
        findViewById(R.id.linearLayout2).setVisibility(View.VISIBLE);
        findViewById(R.id.click_layer).setVisibility(View.INVISIBLE);

        configUI();
        changeSensor(null);
        selectButton(8);
    }

    SensorGroups selectedSensor = null;

    private void getGraphData() {

        final Calendar s_cal = Calendar.getInstance();
        s_cal.set(Calendar.YEAR       ,s_year);
        s_cal.set(Calendar.MONTH      ,s_month);
        s_cal.set(Calendar.DATE       ,s_day);
        s_cal.set(Calendar.HOUR_OF_DAY,s_hour);
        s_cal.set(Calendar.MINUTE     ,s_minute);

        final Calendar e_cal = Calendar.getInstance();
        e_cal.set(Calendar.YEAR       ,e_year);
        e_cal.set(Calendar.MONTH      ,e_month);
        e_cal.set(Calendar.DATE       ,e_day);
        e_cal.set(Calendar.HOUR_OF_DAY,e_hour);
        e_cal.set(Calendar.MINUTE     ,e_minute);

        final String token =  new StorageAPI(this).getBearerToken();
        final String start = ""+s_cal.getTimeInMillis();
        final String end   = ""+e_cal.getTimeInMillis();

        final int data_points = 500;

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Cargando");
        pd.setTitle("Obteniendo datos...");
        pd.show();
        pd.setCancelable(false);

        Log.d("NETWORK", "START 1");
        NetworkAPI.getServiceInstance().getMeasurements(token, selectedSensor.A.sensorId, start, end, data_points, new Callback<Measurement[]>() {
            @Override
            public void success(final Measurement[] measurementsA, Response response) {

                Log.d("NETWORK", "Response: " + measurementsA.length);
                Log.d("NETWORK", "START 2");
                NetworkAPI.getServiceInstance().getMeasurements(token, selectedSensor.B.sensorId, start, end, data_points, new Callback<Measurement[]>() {
                    @Override
                    public void success(final Measurement[] measurementsB, Response response) {
                        if (pd.isShowing()) pd.dismiss();
                        configGraph(measurementsA, measurementsB);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(SensorsBActivity.this, "Error en la conexión.", Toast.LENGTH_SHORT).show();
                        if (pd.isShowing()) pd.dismiss();
                        Log.d("NETWORK", error.getMessage());
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                if (pd.isShowing()) pd.dismiss();
                Toast.makeText(SensorsBActivity.this, "Error en la conexión.", Toast.LENGTH_SHORT).show();
                Log.d("NETWORK", error.getMessage());
            }
        });
    }


    private void selectButton(int index) {
        Button b8    = (Button) findViewById(R.id.b8);
        Button b24   = (Button) findViewById(R.id.b24);
        Button b247  = (Button) findViewById(R.id.b247);
        Button b2430 = (Button) findViewById(R.id.b2430);

        b8.setBackgroundColor(index == 8 ? 0xffbbbbbb : 0xff686568);
        b24.setBackgroundColor(index == 24 ? 0xffbbbbbb : 0xff686568);
        b247.setBackgroundColor(index == 247 ? 0xffbbbbbb : 0xff686568);
        b2430.setBackgroundColor(index == 2430 ? 0xffbbbbbb : 0xff686568);

        b8.setTextColor(index != 8 ? 0xffbbbbbb : 0xff686568);
        b24.setTextColor(index != 24 ? 0xffbbbbbb : 0xff686568);
        b247.setTextColor(index != 247 ? 0xffbbbbbb : 0xff686568);
        b2430.setTextColor(index != 2430 ? 0xffbbbbbb : 0xff686568);
    }


    public void onTurno(View v) {
        configFirstDay(8);
        getGraphData();
        selectButton(8);

    }

    public void onDia(View v) {
        configFirstDay(24);
        getGraphData();
        selectButton(24);
    }

    public void onSemana(View v) {
        configFirstDay(24*7);
        getGraphData();
        selectButton(247);
    }

    public void onMes(View v) {
        configFirstDay(24*30);
        getGraphData();
        selectButton(2430);
    }

    Locale spanish = new Locale("es", "ES");
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy", spanish);

    private void debugWV() {
        if (android.os.Build.VERSION.SDK_INT>=19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    private void configGraph(Measurement[] measurementsA, Measurement[] measurementsB) {

        debugWV();
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.setBackgroundColor(0x00000000);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.d("WEBVIEW", description);
            }
        });

        String html = readFileAsString("graphB.html");
        String img = "x1.png";

        if (selectedSensor != null) img = selectedSensor.image;
        html = html.replace("__IMAGE_SENSOR__", img);

        {
            JSONArray l1X = new JSONArray();
            JSONArray l1Y = new JSONArray();
            JSONArray l2X = new JSONArray();
            JSONArray l2Y = new JSONArray();
            JSONArray mATime = new JSONArray();
            JSONArray mBTime = new JSONArray();

            l1X.put("data1");
            l1Y.put("data2");
            l2X.put("data1");
            l2Y.put("data2");
            mATime.put("x");
            mBTime.put("x");

            int ia = 0;
            int ib = 0;
            int iamax = measurementsA.length;
            int ibmax = measurementsB.length;

            while(true) {
                boolean insertA = false;
                boolean insertB = false;
                Measurement m1 = null;
                Measurement m2 = null;

                if (ia < iamax && ib < ibmax) {
                    m1 = measurementsA[ia];
                    m2 = measurementsB[ib];
                    if (m1.timestamp < m2.timestamp) insertA = true;
                    else insertB = true;
                } else if (ia < iamax) {
                    m1 = measurementsA[ia];
                    insertA = true;
                } else if (ib < ibmax) {
                    m2 = measurementsB[ib];
                    insertB = true;
                } else {
                    break;
                }

                try {

                    if (insertA) {
                        l1X.put(m1.convertedValue.x);
                        l2X.put(m1.convertedValue.y);
                        mATime.put(m1.timestamp);
                        mBTime.put(m1.timestamp);

                        if (ibmax > 0) {
                            int insert = l1Y.length() - 1;
                            if (insert < 1) {
                                l1Y.put(measurementsB[0].convertedValue.x);
                                l2Y.put(measurementsB[0].convertedValue.y);
                            } else {
                                l1Y.put(l1Y.get(insert));
                                l2Y.put(l2Y.get(insert));
                            }
                        } else {
                            l1Y.put(0);
                            l2Y.put(0);
                        }

                        ia++;
                    }

                    if (insertB) {
                        l1Y.put(m2.convertedValue.x);
                        l2Y.put(m2.convertedValue.y);
                        mBTime.put(m2.timestamp);
                        mATime.put(m2.timestamp);

                        if (iamax > 0) {
                            int insert = l1X.length() - 1;
                            if (insert < 1) {
                                l1X.put(measurementsA[0].convertedValue.x);
                                l2X.put(measurementsA[0].convertedValue.y);
                            } else {
                                l1X.put(l1X.get(insert));
                                l2X.put(l2X.get(insert));
                            }
                        } else {
                            l1X.put(0);
                            l2X.put(0);
                        }

                        ib++;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            html = html.replace("line1.data.columns[0] = [];", "line1.data.columns[0] = " + mATime.toString() + ";" );
            html = html.replace("line1.data.columns[1] = [];", "line1.data.columns[1] = " + l1X.toString() + ";");
            html = html.replace("line1.data.columns[2] = [];", "line1.data.columns[2] = " + l1Y.toString() + ";");

            html = html.replace("line2.data.columns[0] = [];", "line2.data.columns[0] = " + mBTime.toString() + ";" );
            html = html.replace("line2.data.columns[1] = [];", "line2.data.columns[1] = " + l2X.toString() + ";");
            html = html.replace("line2.data.columns[2] = [];", "line2.data.columns[2] = " + l2Y.toString() + ";");

            html = html.replace("var run = false;", "var run = true;");

        }

        {
            Integer lastPointH_A = measurementsA.length > 0 ? measurementsA[measurementsA.length - 1].convertedValue.x : 0;
            Integer lastPointV_A = measurementsA.length > 0 ? measurementsA[measurementsA.length - 1].convertedValue.y : 0;
            Integer lastPointH_B = measurementsB.length > 0 ? measurementsB[measurementsB.length - 1].convertedValue.x : 0;
            Integer lastPointV_B = measurementsB.length > 0 ? measurementsB[measurementsB.length - 1].convertedValue.y : 0;

            html = html.replace("content.data.columns[0][1] = 0;", "content.data.columns[0][1] = " + lastPointH_A + ";");
            html = html.replace("content.data.columns[1][1] = 0;", "content.data.columns[1][1] = " + lastPointV_A + ";");
            html = html.replace("content.data.columns[2][1] = 0;", "content.data.columns[2][1] = " + lastPointH_B + ";");
            html = html.replace("content.data.columns[3][1] = 0;", "content.data.columns[3][1] = " + lastPointV_B + ";");
        }

        //Log.d("HTML", html);

        webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);
        webView.setVisibility(View.VISIBLE);
    }

    static int s_year   = 0;
    static int s_month  = 0;
    static int s_day    = 0;
    static int s_hour   = 0;
    static int s_minute = 0;
    static int e_year   = 0;
    static int e_month  = 0;
    static int e_day    = 0;
    static int e_hour   = 0;
    static int e_minute = 0;


    private void configFirstDay (int hours) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, -1*hours);
        s_year     = c.get(Calendar.YEAR);
        s_month    = c.get(Calendar.MONTH);
        s_day      = c.get(Calendar.DAY_OF_MONTH);
        s_hour     = c.get(Calendar.HOUR_OF_DAY);
        s_minute   = c.get(Calendar.MINUTE);

        Button date = (Button) findViewById(R.id.textView5);
        Button time = (Button) findViewById(R.id.textView9);
        date.setText(padding(s_year) + "/" + padding(s_month+1) + "/" + padding(s_day));
        time.setText(padding(s_hour) + ":" + padding(s_minute) );

    }

    private void configUI() {

        configFirstDay(8);
        {
            final Calendar c = Calendar.getInstance();
            e_year = c.get(Calendar.YEAR);
            e_month = c.get(Calendar.MONTH);
            e_day = c.get(Calendar.DAY_OF_MONTH);
            e_hour = c.get(Calendar.HOUR_OF_DAY);
            e_minute = c.get(Calendar.MINUTE);

            Button date = (Button) findViewById(R.id.textView11);
            Button time = (Button) findViewById(R.id.textView10);
            date.setText("" + padding(e_year) + "/" + padding(e_month+1) + "/" + padding(e_day));
            time.setText("" + padding(e_hour) + ":" + padding(e_minute) );
        }
    }


    public void swap(View v) {
        startActivity(new Intent(this, SensorsAActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    static class SensorGroups {
        public Sensor A;
        public Sensor B;
        final public String nameA;
        final public String nameB;
        final public String image;
        SensorGroups(String nameA, String nameB, String image) {
            this.nameA = nameA;
            this.nameB = nameB;
            this.image = image;
        }

        public String getName() {
            String s = "";
            if (A != null && A.displayName != null) s += A.displayName;
            s += " - ";
            if (B != null && B.displayName != null) s += B.displayName;
            return s;
        }
    }

    ArrayList<SensorGroups> sg = new ArrayList<>(8);

    public void changeSensor(View v) {

        try {
            JSONArray ja = new StorageAPI(this).getT1();
            for (int i = 0; i < ja.length(); i++) {
                sg.add(new SensorGroups(
                        ja.getJSONArray(i).getString(0),
                        ja.getJSONArray(i).getString(1),
                        ja.getJSONArray(i).getString(2)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        SensorData.getData(this, new Callback<Sensor[]>() {
            @Override
            public void success(final Sensor[] sensors, Response response) {

                for (SensorGroups aSensorGroup : sg) {
                    for (Sensor aSensor : sensors) {
                        if (aSensorGroup.nameA.equals(aSensor.sensorId)) {
                            aSensorGroup.A = aSensor;
                            if (aSensorGroup.B != null) break;
                        }
                        if (aSensorGroup.nameB.equals(aSensor.sensorId)) {
                            aSensorGroup.B = aSensor;
                            if (aSensorGroup.A != null) break;
                        }
                    }
                }

                final CharSequence[] items = new CharSequence[sg.size()];
                for (int i = 0; i < sg.size(); i++) {
                    items[i] = sg.get(i).getName();
                }

                Integer i = getIntent().getIntExtra("start", -1);
                if (i > 0) {
                    Button b = (Button)findViewById(R.id.changeSensor);
                    selectedSensor = sg.get(i-1);
                    b.setText("Sensor " + selectedSensor.getName());
                    getGraphData();
                    return;
                }

                //if (true) return;

                final Integer[] selected = {0};
                AlertDialog.Builder builder = new AlertDialog.Builder(SensorsBActivity.this);
                builder.setTitle("Sensores")
                        .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selected[0] = which;
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Button b = (Button)findViewById(R.id.changeSensor);
                                selectedSensor = sg.get(selected[0]);
                                b.setText("Sensor " + selectedSensor.getName());
                                getGraphData();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                //...
                            }
                        });

                builder.create().show();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void time1(View v) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.sba = this;
        newFragment.show(getFragmentManager(), "timePicker");
        yesterday = true;
    }

    public void date1(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.sba = this;
        newFragment.show(getFragmentManager(), "datePicker");
        yesterday = true;
    }

    public void time2(View v) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.sba = this;
        newFragment.show(getFragmentManager(), "timePicker");
        yesterday = false;
    }

    public void date2(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.sba = this;
        newFragment.show(getFragmentManager(), "datePicker");
        yesterday = false;
    }

    static boolean yesterday = true;

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        private SensorsBActivity sba;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (yesterday) {
                return new TimePickerDialog(getActivity(), this, s_hour, s_minute, DateFormat.is24HourFormat(getActivity()));
            } else {
                return new TimePickerDialog(getActivity(), this, e_hour, e_minute, DateFormat.is24HourFormat(getActivity()));
            }
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            if (yesterday) {
                s_hour = hourOfDay;
                s_minute = minute;
                Button time = (Button) getActivity().findViewById(R.id.textView9);
                time.setText( padding(hourOfDay) + ":" + padding(minute) );
            } else {
                e_hour = hourOfDay;
                e_minute = minute;
                Button time = (Button) getActivity().findViewById(R.id.textView10);
                time.setText( padding(hourOfDay) + ":" + padding(minute) );
            }
            sba.getGraphData();
        }
    }

    static public String padding(int n) {
        return n < 10 ? "0"+n : ""+n;
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        public SensorsBActivity sba;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            if (yesterday) {
                return new DatePickerDialog(getActivity(), this, s_year, s_month, s_day);
            } else {
                return new DatePickerDialog(getActivity(), this, e_year, e_month, e_day);
            }
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            if (view.isShown()) {
                if (yesterday) {
                    s_year = year;
                    s_month = month;
                    s_day = day;
                    Button date = (Button) getActivity().findViewById(R.id.textView5);
                    date.setText(padding(year) + "/" + padding(month+1) + "/" + padding(day));
                } else {
                    e_year = year;
                    e_month = month;
                    e_day = day;
                    Button date = (Button) getActivity().findViewById(R.id.textView11);
                    date.setText(padding(year) + "/" + padding(month+1) + "/" + padding(day));
                }
                sba.getGraphData();
            }
        }
    }


}
