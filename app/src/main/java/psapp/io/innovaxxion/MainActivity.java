package psapp.io.innovaxxion;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import psapp.io.innovaxxion.model.Measurement;
import psapp.io.innovaxxion.model.Sensor;
import psapp.io.innovaxxion.network.NetworkAPI;
import psapp.io.innovaxxion.network.SensorData;
import psapp.io.innovaxxion.storage.StorageAPI;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hideActionBar();

        Intent i = new Intent(this, SensorsAActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }

    public void goToSensorsA(View view) {
        Intent i = new Intent(this, SensorsAActivity.class);
        startActivity(i);
    }

    public void goToSensorsB(View view) {
        Intent i = new Intent(this, SensorsBActivity.class);
        startActivity(i);
    }

    public static void checkIfUnauthorized(RetrofitError error, Activity activity) {
        if (error.getResponse().getStatus() == 401) {
            logOut(activity);
        }
    }
    public static void logOut(Activity context) {
        new StorageAPI(context).clear();
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void hideActionBar() {
        ActionBar actionBar = getActionBar();

        if (null != actionBar)
            actionBar.hide();
    }

    private void initSensorList(Sensor[] sensors) {
        ListView listSensors = (ListView) findViewById(R.id.listSensors);
        AdapterView.OnItemClickListener clickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String sersorIdSelected = (String) parent.getAdapter().getItem(position);
                ((EditText) findViewById(R.id.editSensorId)).setText(sersorIdSelected);
            }
        };
        ArrayList<String> sensorsId = new ArrayList<String>();
        for (Sensor sensor : sensors) {
            String name = sensor.displayName != null ? sensor.displayName : sensor.sensorId;
            sensorsId.add(name);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, sensorsId);

        listSensors.setAdapter(adapter);
        listSensors.setOnItemClickListener(clickListener);
    }

    public void getMeasurements(View v) {
        String id = ((TextView) findViewById(R.id.editSensorId)).getText().toString();
        String dateStart = ((TextView) findViewById(R.id.editStartDate)).getText().toString();
        String dateEnd = ((TextView) findViewById(R.id.editEndDate)).getText().toString();
        String bearerToken = new StorageAPI(this).getBearerToken();

        dateStart = dateStart.equals("") ? new DateTime(1970, 1, 1, 0, 0).toDateTimeISO().toString() : dateStart;
        dateEnd = dateEnd.equals("") ? new DateTime(2050, 1, 1, 0, 0).toDateTimeISO().toString() : dateEnd;

        NetworkAPI.getServiceInstance().getMeasurements(bearerToken, id, "\""+dateStart+"\"", "\""+dateEnd+"\"", 100,
                new Callback<Measurement[]>() {
                    @Override
                    public void success(Measurement[] measurements, Response response) {
                        Log.d("NETWORK", response.getUrl());
                        String d = "";
                        for (Measurement m : measurements) {
                            d = d.concat("[" + m.convertedValue.x + ", " + m.convertedValue.y + "]" + (System.getProperty("line.separator")));
                        }
                        Toast.makeText(MainActivity.this, d, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, error.getMessage());
                        checkIfUnauthorized(error, MainActivity.this);
                    }
                });
    }
}
