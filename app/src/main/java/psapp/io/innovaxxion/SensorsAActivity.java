package psapp.io.innovaxxion;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import psapp.io.innovaxxion.model.Measurement;
import psapp.io.innovaxxion.network.NetworkAPI;
import psapp.io.innovaxxion.storage.StorageAPI;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SensorsAActivity extends SensorBaseActivity {

    private HashMap<String, SensorPosConfig> sensorMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors_a);

        findViewById(R.id.linearLayout1).setVisibility(View.GONE);
        findViewById(R.id.linearLayout2).setVisibility(View.GONE);
        findViewById(R.id.click_layer).setVisibility(View.INVISIBLE);

        configSensorMap();
        setGraphData(null);

    }

    Timer timer = null;
    @Override
    protected void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                Log.d("Network", "Update your data " + (new Date().getTime()/1000));
                updateNetworkData();
            }
        }, 0, 15000);
    }

    private void updateNetworkData() {
        NetworkAPI.getServiceInstance().getMeasurementsAll(new StorageAPI(this).getBearerToken(), new Callback<Measurement[]>() {
            @Override
            public void success(Measurement[] measurements, Response response) {
                setGraphData(measurements);
            }

            @Override
            public void failure(RetrofitError error) {
                MainActivity.checkIfUnauthorized(error, SensorsAActivity.this);
            }
        });
    }

    public void clickBox(Integer i){
//        Intent intent = new Intent(this, SensorsBActivity.class);
//        intent.putExtra("start", i);
//        startActivity(intent);
//        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    public void box1(View v) { clickBox(1); }
    public void box2(View v) { clickBox(2); }
    public void box3(View v) { clickBox(3); }
    public void box4(View v) { clickBox(4); }
    public void box5(View v) { clickBox(5); }
    public void box6(View v) { clickBox(6); }
    public void box7(View v) { clickBox(7); }
    public void box8(View v) { clickBox(8); }


    private void configSensorMap() {

        try {
            JSONArray ja = new StorageAPI(this).getT2();
            for (int i = 0; i < ja.length(); i++) {
                sensorMap.put(
                        ja.getJSONArray(i).getString(0),
                        new SensorPosConfig(
                                ja.getJSONArray(i).getInt(1),
                                ja.getJSONArray(i).getString(2)
                        ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static class SensorPosConfig {
        public final Integer position;
        public final String group;

        SensorPosConfig(Integer position, String group) {
            this.position = position;
            this.group = group;
        }
    }

    private String toJSON(ArrayList list, int limit) {
        String output = "[";
        boolean coma = false;
        int i = 0;
        for(Object o : list) {


            if (o instanceof String) {
                if (coma) output += ", ";

                output += "\"" + (String)o + "\"" ;
            } else if (o instanceof Integer) {
                if (coma) output += ", ";
                output += "" + (Integer)o;
            }

            coma = true;
            if (limit > 0 && limit <= i) {
                break;
            } else {
                i++;
            }
        }
        output += "]";
        return output;
    }


    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void changeScreen1() { changeScreen(1); }
        @JavascriptInterface
        public void changeScreen2() { changeScreen(2); }
        @JavascriptInterface
        public void changeScreen3() { changeScreen(3); }
        @JavascriptInterface
        public void changeScreen4() { changeScreen(4); }
        @JavascriptInterface
        public void changeScreen5() { changeScreen(5); }
        @JavascriptInterface
        public void changeScreen6() { changeScreen(6); }
        @JavascriptInterface
        public void changeScreen7() { changeScreen(7); }
        @JavascriptInterface
        public void changeScreen8() { changeScreen(8); }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void changeScreen(Integer i) {
            Intent intent = new Intent(SensorsAActivity.this, SensorsBActivity.class);
            intent.putExtra("start", i);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void setGraphData(Measurement[] measurements) {
        final WebView webView = new WebView(this);
        //WebView webView = (WebView) findViewById(R.id.webView);
        //if (null==webView) return;

        webView.setBackgroundColor(0x00000000);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.d("WEBVIEW", description);
            }
        });

        String html = readFileAsString("graphA.html");
        webView.getSettings().setJavaScriptEnabled(true);

        if (measurements == null) {
            webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
            return;
        }

        /// CONVERT AND PARSE MEASUREMENTS[] FOR THE GRAPHS.

        ArrayList<String>  sensor_name_A    = new ArrayList<String>(8);
        ArrayList<String>  value_display_A  = new ArrayList<String>(8);
        ArrayList<Integer> real_data_A_H    = new ArrayList<Integer>(8);
        ArrayList<Integer> real_data_A_W    = new ArrayList<Integer>(8);
        ArrayList<String>  sensor_name_B    = new ArrayList<String>(8);
        ArrayList<String>  value_display_B  = new ArrayList<String>(8);
        ArrayList<Integer> real_data_B_H    = new ArrayList<Integer>(8);
        ArrayList<Integer> real_data_B_W    = new ArrayList<Integer>(8);

        for (int i = 0; i < 8; i++) {
            sensor_name_A  .add(i, "");
            value_display_A.add(i, "");
            real_data_A_H  .add(i, 0);
            real_data_A_W  .add(i, 0);
            sensor_name_B  .add(i, "");
            value_display_B.add(i, "");
            real_data_B_H  .add(i, 0);
            real_data_B_W  .add(i, 0);
        }

        for (Measurement measurement : measurements) {

            SensorPosConfig spc = sensorMap.get(measurement.sensor.sensorId);
            if (spc == null) continue;

            int w = measurement.convertedValue.x;
            int h = measurement.convertedValue.y;

            if (spc.group.equals("A")) {
                real_data_A_H.set(spc.position, h);
                real_data_A_W.set(spc.position, w);
                value_display_A.set(spc.position,  "V: " + w + ",  H: " + h);
                sensor_name_A.set(spc.position, measurement.sensor.displayName != null ? measurement.sensor.displayName : measurement.sensor.sensorId);
            } else if (spc.group.equals("B")) {
                real_data_B_H.set(spc.position, h);
                real_data_B_W.set(spc.position, w);
                value_display_B.set(spc.position,  "V: " + w + ",  H: " + h);
                sensor_name_B.set(spc.position, measurement.sensor.displayName != null ? measurement.sensor.displayName : measurement.sensor.sensorId);
            }
        }

        html = html.replace("var sensor_name_A   = [];", "var sensor_name_A   = " + toJSON(sensor_name_A, 8) + " ;");
        html = html.replace("var value_display_A = [];", "var value_display_A = " + toJSON(value_display_A, 8) + ";");
        html = html.replace("var real_data_A_H   = [];", "var real_data_A_H   = " + toJSON(real_data_A_H, 8) + ";");
        html = html.replace("var real_data_A_W   = [];", "var real_data_A_W   = " + toJSON(real_data_A_W, 8) + ";");

        html = html.replace("var sensor_name_B   = [];", "var sensor_name_B   = " + toJSON(sensor_name_B, 8) + " ;");
        html = html.replace("var value_display_B = [];", "var value_display_B = " + toJSON(value_display_B, 8) + ";");
        html = html.replace("var real_data_B_H   = [];", "var real_data_B_H   = " + toJSON(real_data_B_H, 8) + ";");
        html = html.replace("var real_data_B_W   = [];", "var real_data_B_W   = " + toJSON(real_data_B_W, 8) + ";");
        html = html.replace("var run = false;", "var run = true;");

        webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
        webView.setId(R.id.webView);
        //webView.setVisibility(View.INVISIBLE);

        final View v = findViewById(R.id.webView);
        final ViewGroup parent = (ViewGroup) v.getParent();
        int index = parent.indexOfChild(v);

//        parent.addView(v, -1);
        parent.addView(webView, index);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("WEB", "remove");
                parent.removeView(v);
            }
        }, 1000);




    }

    public void swap(View v) {
        startActivity(new Intent(this, SensorsBActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

}
