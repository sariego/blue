package psapp.io.innovaxxion.network;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import psapp.io.innovaxxion.MainActivity;
import psapp.io.innovaxxion.model.Sensor;
import psapp.io.innovaxxion.network.NetworkAPI;
import psapp.io.innovaxxion.storage.StorageAPI;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by pedro on 12/20/14.
 */
public class SensorData {

    public static void getData(final Activity context, final Callback<Sensor[]> cb) {

        Sensor[] s = new StorageAPI(context).getSensors();
        if (s != null) {
            cb.success(s, null);
            return;
        }

        final ProgressDialog pd = new ProgressDialog(context);
        pd.setTitle("Sensores");
        pd.setMessage("Cargando");
        pd.show();

        NetworkAPI.getServiceInstance().getSensors(new StorageAPI(context).getBearerToken(), new Callback<Sensor[]>() {
            @Override
            public void success(Sensor[] sensors, Response response) {
                if (pd.isShowing()) pd.dismiss();
                cb.success(sensors, response);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("ERROR: ", "" + error.getMessage());
                MainActivity.checkIfUnauthorized(error, context);
                if (pd.isShowing()) pd.dismiss();
                cb.failure(error);
            }
        });


    }
}
