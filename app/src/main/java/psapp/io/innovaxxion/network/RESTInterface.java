package psapp.io.innovaxxion.network;

import com.google.gson.JsonObject;

import psapp.io.innovaxxion.model.Measurement;
import psapp.io.innovaxxion.model.Sensor;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

public interface RESTInterface {


    @FormUrlEncoded
    @POST("/auth/local")
    void postLogin(@Field("email") String email,
                   @Field("password") String pass,
                   Callback<JsonObject> cb);

    @GET("/api/users/me")
    void getMe(@Header("Authorization") String token, Callback<JsonObject> cb);

    @GET("/api/sensors")
    void getSensors(@Header("Authorization") String token,
                    Callback<Sensor[]> cb);

    @GET("/api/measurements")
    void getMeasurements(@Header("Authorization") String token,
                         @Query("sensorId") String sensorId,
                         @Query("startDate") String startDate,
                         @Query("endDate") String endDate,
                         @Query("limit") Integer limit,
                         Callback<Measurement[]> cb);

    @GET("/api/measurements/all")
    void getMeasurementsAll(@Header("Authorization") String token,
                            Callback<Measurement[]> cb);
}
