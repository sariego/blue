package psapp.io.innovaxxion.network;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class NetworkAPI {

    private final static String endpoint = "http://innovaxxion.api-psapp.com:9000";

    private static RESTInterface instance;

    public static RESTInterface getServiceInstance() {
        if (null == instance)
            instance =  new RestAdapter.Builder()
                .setClient(new OkClient(new OkHttpClient()))
                //.setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(endpoint)
                .build()
                .create(RESTInterface.class);
        return instance;
    }
}
