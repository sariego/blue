package psapp.io.innovaxxion.model;

import com.google.gson.annotations.SerializedName;

public class Measurement {
    public String _id;
    public String conversionScale;
    public Value value;
    public Sensor sensor;
    public Long timestamp;
    //public String timeName;
    public ConvertedValue convertedValue;
    public String id;

    public class Value {
        public Double PX;
        public Double PY;
    }

    public class ConvertedValue {
        @SerializedName("y[mm]")
        public Integer y;
        @SerializedName("x[mm]")
        public Integer x;
    }
}
