package psapp.io.innovaxxion.model;

public class Sensor {
    public String _id;
    public String sensorId;
    public String displayName;
    public Double latitude;
    public Double longitude;
    public Extras extras;

    class Extras {
        public Integer net;
    }
}
