package psapp.io.innovaxxion.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.Date;

import psapp.io.innovaxxion.model.Sensor;

public class StorageAPI {

    private SharedPreferences prefs;

    public StorageAPI(Context context) {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clear() {
        prefs.edit().clear().commit();
    }


    public void saveSensor(Sensor[] sensors) {

        SharedPreferences.Editor editor = prefs.edit();
        editor
                .putString("sensors", new Gson().toJson(sensors))
                .putString("sensors_date", ""+new Date().getTime())
                .commit();

    }

    public Sensor[] getSensors() {

        String data = prefs.getString("sensors", null);
        if (data == null) return null;

        String date = prefs.getString("sensors_date", null);
        if (date == null) return null;

        Long dateUT = Long.parseLong(date);
        Long now = new Date().getTime();
        Long mSec = 1000l * 60l;
        if (now - dateUT > mSec) {
            return new Gson().fromJson(data, Sensor[].class);
        } else {
            return null;
        }
    }


    public void saveToken(String token) {
        prefs.edit()
                .putString("token", token)
                .putString("bearerToken", "Bearer " + token)
                .commit();
    }
    public void savet1(String t1) {
        prefs.edit()
                .putString("t1", t1)
                .commit();
    }
    public void savet2(String t2) {
        prefs.edit()
                .putString("t2", t2)
                .commit();
    }


    public void clearToken() {
        prefs.edit()
                .remove("token")
                .remove("t1")
                .remove("t2")
                .remove("bearerToken")
                .commit();
    }

    public JSONArray getT1() {
        try {
            return new JSONArray(prefs.getString("t1", null));
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }
    public JSONArray getT2() {
        try {
            return new JSONArray(prefs.getString("t2", null));
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public String getBearerToken() {
        return prefs.getString("bearerToken", null);
    }
}
